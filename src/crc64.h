
#ifndef _CRC64_H
#define _CRC64_H

#include <inttypes.h>
#include <stdlib.h>


extern uint64_t crc64(uint64_t crc, const unsigned char *s, uint64_t l);

#endif /* _CRC64_H */

